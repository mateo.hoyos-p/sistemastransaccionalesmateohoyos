package cache;

import java.util.HashMap;
import java.util.Map;

public class CacheBranch {
    private Map<Integer, String> sucursalesCache;

    public CacheBranch() {
        sucursalesCache = new HashMap<>();
    }

    public String getSucursal(int id) {
        return sucursalesCache.get(id);
    }

    public void addSucursal(int id, String sucursal) {
        sucursalesCache.put(id, sucursal);
    }

    public void removeSucursal(int id) {
        sucursalesCache.remove(id);
    }
}
