package cache;

import java.util.HashMap;
import java.util.Map;

public class CacheProduct {
    private Map<Integer, String> productosCache;

    public CacheProduct() {
        productosCache = new HashMap<>();
    }

    public String getProducto(int id) {
        return productosCache.get(id);
    }

    public void addProducto(int id, String producto) {
        productosCache.put(id, producto);
    }

    public void removeProducto(int id) {
        productosCache.remove(id);
    }
}
