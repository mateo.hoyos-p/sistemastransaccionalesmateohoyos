package main;

import BD.DatabaseConnection;
import cache.CacheProduct;
import cache.CacheBranch;
import messaging.Productor;
import messaging.Consumidor;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class main {
    public static void main(String[] args) {
        try {
            // Conectar a la base de datos
            Connection conn = DatabaseConnection.getConnection();
            System.out.println("Conexión a la base de datos establecida.");
            
            // Insertar datos de prueba (opcional)
            insertarDatosDePrueba(conn);

            // Crear instancia de Cache
            CacheProduct cacheProduct = new CacheProduct();
            CacheBranch cacheBranch = new CacheBranch();

            // Agregar productos y sucursales a la cache
            cacheProduct.addProducto(1, "Blue Yeti");
            cacheBranch.addSucursal(1, "NJ");

            // Enviar mensaje con ActiveMQ
            Productor productor = new Productor();
            productor.enviarMensaje("Transacción de ejemplo enviada");

            // Recibir mensaje con ActiveMQ
            Consumidor consumidor = new Consumidor();
            consumidor.recibirMensaje();

        } catch (SQLException e) {
            System.err.println("Error al conectar con la base de datos: " + e.getMessage());
        }
    }

    private static void insertarDatosDePrueba(Connection conn) {
        try {
            Statement stmt = conn.createStatement();
            
            
            String sqlInsert1 = "INSERT INTO sucursales (nombre, direccion, telefono) VALUES ('TX', '12 Main St', '101')";
            stmt.executeUpdate(sqlInsert1);

            String sqlInsert2 = "INSERT INTO productos (nombre, descripcion, precio) VALUES ('Rode NT1', 'Micro profesional', 199.99)";
            stmt.executeUpdate(sqlInsert2);

            System.out.println("Datos de prueba insertados.");
            
        } catch (SQLException e) {
            System.err.println("Error al insertar datos de prueba: " + e.getMessage()); }}
}