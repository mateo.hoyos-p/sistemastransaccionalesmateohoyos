package messaging;

import org.apache.activemq.ActiveMQConnectionFactory;
import jakarta.jms.*;

public class Productor {

    public void enviarMensaje(String mensaje) {
        ConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        try (Connection connection = factory.createConnection()) {
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destino = session.createQueue("colaTransacciones");

            MessageProducer productor = session.createProducer(destino);
            TextMessage textMessage = session.createTextMessage(mensaje);

            productor.send(textMessage);
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
